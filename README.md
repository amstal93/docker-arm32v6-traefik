# Raspberry Pi 1/0 Traefik v1.7.7 Docker Image With Alpine Linux

[![pipeline status](https://gitlab.com/offtechnologies/docker-arm32v6-traefik/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/docker-arm32v6-traefik/commits/master)
[![This image on DockerHub](https://img.shields.io/docker/pulls/offtechnologies/docker-arm32v6-traefik.svg)](https://hub.docker.com/r/offtechnologies/docker-arm32v6-traefik/)
[![](https://images.microbadger.com/badges/version/offtechnologies/docker-arm32v6-traefik.svg)](https://microbadger.com/images/offtechnologies/docker-arm32v6-traefik "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/offtechnologies/docker-arm32v6-traefik.svg)](https://microbadger.com/images/offtechnologies/docker-arm32v6-traefik "Get your own image badge on microbadger.com")


[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Raspberry Pi 1/0 compatible Docker base image with Alpine Linux and Traefik.

## Usage:

```
sudo docker run --name traefik -d \
  -p 8080:8080 \
  -p 80:80 \
  -p  443:443 \
  -v $PWD/traefik.toml:/etc/traefik/traefik.toml \
  registry.gitlab.com/offtechnologies/docker-arm32v6-traefik:master
```

## Credits
- [klud1/rpi-traefik](https://github.com/klud1/rpi-traefik)
